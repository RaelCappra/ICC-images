#!/usr/bin/env python3
import sys
from matrices import scalarProduct2d
if sys.version_info[0] < 3:
    print("Not supported in python < 3")
    exit(1)

class Image:
    def __init__(self, data, scale):
        #TODO(Rael): error check
        #assert(data is list)
        #assert(data is not empty)
        #assert(data[i] is list for all i)
        #[x]assert(len(data[i]) == len(data[j]))
        #assert(only numbers in data)
        self.data = data
        self._scale = scale
        self._height = len(data)
        self._width = len(data[0])
        l = len(data[0])
        #for linha in data:
        #    #print(len(linha))
        #    assert(len(linha) == l)


    @property
    def scale(self):
        return self._scale

    @scale.setter
    def scale(self, scale):
        if scale <= 0:
            raise ValueError("scale must be positive")
        scale = int(scale)
        ratio = scale / float(self._scale)
        for i in range(self.height):
            for j in range(self.width):
                red = min(scale, int(self.data[i][j][0] * ratio))
                green = min(scale, int(self.data[i][j][1] * ratio))
                blue = min(scale, int(self.data[i][j][2] * ratio))
                self.data[i][j] = (red, green, blue)
        self._scale = scale
    
    @property
    def width(self):
        return self._width

    @property
    def height(self):
        return self._height

    def clear(self, rate=.2):
        scale = self.scale
        for i in range(self._height):
            for j in range(self._width):
                pixel = self.data[i][j]
                r = min(pixel[0] + int(scale * rate), scale)
                g = min(pixel[1] + int(scale * rate), scale)
                b = min(pixel[2] + int(scale * rate), scale)
                self.data[i][j] = (r, g, b)
                #self.data[i][j] = min(pixel + int(scale * rate), scale)

    def reverseColor(self):
        for i in range(self._height):
            for j in range(self._width):
                pixel = self.data[i][j]
                r = self.scale - pixel[0]
                g = self.scale - pixel[1]
                b = self.scale - pixel[2]
                self.data[i][j] = (r, g, b)

    def correct(self):
        #r = 1.0 / 8
        #self.convolute([
        #    [r, r, r],
        #    [r, 0, r],
        #    [r, r, r]
        #    ])
       for i in range(1, self._height - 1):
           for j in range(1, self._width - 1):
               #print("%d %d" %(i, j))
               rgb = []
               for c in range(3):
                   channel = int((\
                           self.data[i + 1][j][c] +
                           self.data[i][j + 1][c] +
                           self.data[i - 1][j][c] +
                           self.data[i][j - 1][c] +
                           self.data[i + 1][j + 1][c] +
                           self.data[i + 1][j - 1][c] +
                           self.data[i - 1][j + 1][c] +
                           self.data[i- 1 ][j - 1][c]) / 8.0)
                   rgb.append(channel)
               self.data[i][j] = tuple(rgb)
    def rotateRight(self):
        newData = []
        for i in range(self._width):
            newData.append([])
        for i in range(self._height):
            linha = self.data[i]
            for j in range(self._width):
                pixel = linha[j]
                newData[j] = [pixel] + newData[j] #append(pixel)
        self.data = newData
        self._width, self._height = self._height, self._width;

    def rotateLeft(self):
        newData = []
        for i in range(self._width):
            newData.append([])
        for i in range(self._height):
            linha = self.data[i]
            for j in range(self._width):
                pixel = linha[j]
                newData[self._width - j - 1].append(pixel)
        self.data = newData
        self._width, self._height = self._height, self._width;

    def rotate180(self):
        self.rotateRight()
        self.rotateRight()

    def flipHorizontal(self):
        newData = []
        for i in range(self._height):
            newData.append([])
        for i in range(self._height):
            linha = self.data[i]
            for j in range(self._width):
                pixel = linha[self._width - j - 1]
                newData[i].append(pixel)
        self.data = newData

    def flipVertical(self):
        newData = []
        for i in range(self._height):
            newData.append([])
        for i in range(self._height):
            linha = self.data[i]
            for j in range(self._width):
                pixel = linha[j]
                newData[self._height - i - 1].append(pixel) 
        self.data = newData
#TODO:adapt
    def fun(self):
        import random
        for i in range(self._height):
            for j in range(self._width):
                pixel = self.data[i][j]
                self.data[i][j] = min(pixel + random.randint(-250, 250), self.scale)
                self.data[i][j] = max(self.data[i][j], 0)

        self.reverseColor()
        self.flipVertical()
#TODO:arrumar essas convoluções
#TODO:máscara de tamanho diferente
    def convolute(self, mask):
        """3x3 mask only"""
        newData = [list(self.data[0])]
        for i in range(1, self._height - 1):
            newData.append(list(self.data[i]))
            for j in range(1, self._width - 1):
                rgb = []
                for c in range(3):
                    channel = int(\
                            self.data[i - 1][j - 1][c] * mask[0][0] +
                            self.data[i - 0][j - 1][c] * mask[1][0] +
                            self.data[i + 1][j - 1][c] * mask[2][0] +
                            self.data[i - 1][j - 0][c] * mask[0][1] +
                            self.data[i - 0][j - 0][c] * mask[1][1] +
                            self.data[i + 1][j - 0][c] * mask[2][1] +
                            self.data[i - 1][j + 1][c] * mask[0][2] +
                            self.data[i - 0][j + 1][c] * mask[1][2] +
                            self.data[i + 1][j + 1][c] * mask[2][2])
                    channel = min(channel, self.scale)
                    channel = max(channel, 0)
                    rgb.append(channel)
                newData[i][j] = tuple(rgb)
        newData.append(list(self.data[self._height - 1]))
        assert len(newData) == len(self.data)
        assert len(newData[0]) == len(self.data[0])
        assert len(newData[self._height - 1]) == len(self.data[self._height - 1])
        self.data = newData

    def sobel_convolute(self, maskx, masky):
        """3x3 mask only"""
        newData = [list(self.data[0])]
        for i in range(1, self._height - 1):
            newData.append(list(self.data[i]))
            for j in range(1, self._width - 1):
                rgb = []
                for c in range(3):
                    channelx = int(\
                            self.data[i - 1][j - 1][c] * maskx[0][0] +
                            self.data[i - 0][j - 1][c] * maskx[1][0] +
                            self.data[i + 1][j - 1][c] * maskx[2][0] +
                            self.data[i - 1][j - 0][c] * maskx[0][1] +
                            self.data[i - 0][j - 0][c] * maskx[1][1] +
                            self.data[i + 1][j - 0][c] * maskx[2][1] +
                            self.data[i - 1][j + 1][c] * maskx[0][2] +
                            self.data[i - 0][j + 1][c] * maskx[1][2] +
                            self.data[i + 1][j + 1][c] * maskx[2][2])
                    #channelx = min(channelx, self.scale)
                    #channelx = max(channelx, 0)

                    channely = int(\
                            self.data[i - 1][j - 1][c] * masky[0][0] +
                            self.data[i - 0][j - 1][c] * masky[1][0] +
                            self.data[i + 1][j - 1][c] * masky[2][0] +
                            self.data[i - 1][j - 0][c] * masky[0][1] +
                            self.data[i - 0][j - 0][c] * masky[1][1] +
                            self.data[i + 1][j - 0][c] * masky[2][1] +
                            self.data[i - 1][j + 1][c] * masky[0][2] +
                            self.data[i - 0][j + 1][c] * masky[1][2] +
                            self.data[i + 1][j + 1][c] * masky[2][2])
                    #channely = min(channely, self.scale)
                    #channely = max(channely, 0)
                    
                    channel = int((channelx*channelx + channely*channely) ** (.5))
                    channel = min(channel, self.scale)
                    #channel = max(channel, 0)
                    rgb.append(channel)
                newData[i][j] = tuple(rgb)
        newData.append(list(self.data[self._height - 1]))
        assert len(newData) == len(self.data)
        assert len(newData[0]) == len(self.data[0])
        assert len(newData[self._height - 1]) == len(self.data[self._height - 1])
        self.data = newData
    
    def blur(self):
        self.convolute([
            [0, .2, 0],
            [.2, .2, .2],
            [0, .2, 0]
            ])

    def border(self):
        self.convolute([
            [0, 1, 0],
            [1, -4, 1],
            [0, 1, 0]
            ])

    def sobel(self):
        self.sobel_convolute([
            [-1, 0, 1],
            [-2, 0, 2],
            [-1, 0, 1]
            ],
            [
            [-1, -2, -1],
            [0, 0, 0],
            [1, 2, 1]
            ])

    def relief(self):
        self.convolute([
            [-2, -1, 0],
            [-1, 1, 1],
            [0, 1, 2]
            ])

    def merge(self, other):
        assert self.height == other.height and self.width == other.width
        oldScale = self.scale
        self.scale = other.scale
        for i in range(self.height):
            for j in range(self.width):
                red = self.data[i][j][0] / 2.0 + other.data[i][j][0] / 2.0
                green = self.data[i][j][1] / 2.0 + other.data[i][j][1] / 2.0
                blue = self.data[i][j][2] / 2.0 + other.data[i][j][2] / 2.0
                self.data[i][j] = (int(red), int(green), int(blue))

actions = {
        "border": Image.border,
        "blur": Image.blur,
        "fun": Image.fun,
        "rotater": Image.rotateRight,
        "rotatel": Image.rotateLeft,
        "rotate180": Image.rotate180,
        "clear": Image.clear,
        "correct": Image.correct,
        "reverse": Image.reverseColor,
        "fliph": Image.flipHorizontal,
        "flipv": Image.flipVertical,
        "relief": Image.relief,
        "convolute": Image.convolute,
        "sobel": Image.sobel,
        }
VALID_ACTIONS = actions.keys()
def usageError():
    valid = ""
    for i in VALID_ACTIONS:
        valid += i
        valid += "|"
    valid = valid[:-1]
    print("USAGE: python %s %s [input_file|-] [output_file|-]" % (sys.argv[0], valid))
    exit(1)

def oneDimensionTo2Dimensions(l, width, height):
    result = []
    listIndex = 0
    for i in range(height):
        line = []
        for j in range(width):
            if listIndex >= len(l):
                for k in range(j, width):
                    line.append((0, 0, 0))
                result.append(line)
                return result
            line.append(l[listIndex])
            listIndex += 1
        result.append(line)       
    return result

def readImageFrom(f):
    binary = False
    mode = 0 #0 - mono; 1 - gray; 2 - color
    header = f.readline().decode("ascii")
    if header.find("P6") != -1 or\
            header.find("P5") != -1 or\
            header.find("P4") != -1:
                binary = True
    if header.find("P2") != -1 or header.find("P5") != -1:
        mode = 1
    elif header.find("P3") != -1 or header.find("P6") != -1:
        mode = 2

    dim = f.readline().decode('ascii')
    dim = dim.rstrip("\n")
    dim = dim.rstrip(" ")
    dim = dim.split(' ')

    dim = (int(dim[0]), int(dim[1]))
    if(mode):
        scale = int(f.readline())
    else:
        scale = 1

    data = []
    rgb = []
    if binary:
        byte = f.read(1)
        while byte != b'':
            value = byte[0]
            if len(rgb) >= 3:
                data.append(tuple(rgb))
                rgb = []

            if value in range(scale + 1):
                rgb.append(value)
                if(mode < 2):
                    rgb.append(value)
                    rgb.append(value)
            byte = f.read(1)

        if (rgb): #se ficou um pixel com canais faltando
            for i in range(3 - len(rgb)):
                rgb.append(0)
            data.append(tuple(rgb))
    else:
        byte = f.read(1)
        buff = ""
        while byte != b'':
            char = byte.decode('ascii')
            if char in ('0', '1', '3', '2', '4', '5', '6', '7', '8', '9'):
                buff += char
                byte = f.read(1)
                continue
            elif len(buff) > 0:
                #print("buff:"+buff)
                value = int(buff)
                buff = ""
            else:
                byte = f.read(1)
                continue

            if len(rgb) >= 3:
                data.append(tuple(rgb))
                rgb = []

            if value in range(scale + 1):
                rgb.append(value)
                if(mode < 2):#se nao for colorido, r, g e b são iguais
                    rgb.append(value)
                    rgb.append(value)
            byte = f.read(1)

        if (rgb): #se ficou um pixel com canais faltando
            for i in range(3 - len(rgb)):
                rgb.append(rgb[0])
            data.append(tuple(rgb))
    
    #print(len(data) == dim[0] * dim[1])
    data = oneDimensionTo2Dimensions(data, dim[0], dim[1])
    #for i in data:
    #    print(i)
    #print(len(data) == dim[1])
    #print(len(data[0]) == dim[0])
    #print(len(data[-1]) == dim[0])
    image = Image(data, scale)
    return image

def writeImageTo(f, image):
    f.write(b'P6\n')

    s = "%d %d\n" % (image.width, image.height)
    f.write(s.encode('ascii'))

    scale = image.scale
    s = "%d\n" % scale
    f.write(s.encode('ascii'))
    for linha in image.data:
        for pixel in linha:
            f.write(bytes(pixel))
            #f.write(chr(pixel[0]))
            #f.write(chr(pixel[1]))
            #f.write(chr(pixel[2]))
            #novaLinha += "%d%d%d" % pixel

if __name__ == '__main__':
    if len(sys.argv) < 2:
        usageError()

    action = sys.argv[1].lower()
    if action not in VALID_ACTIONS:
        usageError()
    if len(sys.argv) < 3:
        sys.argv.append('-')
    if len(sys.argv) < 4:
        sys.argv.append('-')

    #filename = input()
    filename = sys.argv[2]
    data = []
    if filename == "-":
        image = readImageFrom(sys.stdin.buffer)
    else:
        with open(filename, "rb") as f:
            image = readImageFrom(f)

    #TODO(Rael): corrigir essa gambi pra convolução custom
    if action != 'convolute':
        args = ()
    else:
        args = ([
            [1.0/8, 1.0/8, 1.0/8],
            [1.0/8, 0, 1.0/8],
            [1.0/8, 1.0/8, 1.0/8]],)
    actions[action](image, *args)

    #saida = input()
    saida = sys.argv[3]
    if saida == "-":
        writeImageTo(sys.stdout.buffer, image)
    else:
        with open(saida, "wb") as f:
            writeImageTo(f, image)
