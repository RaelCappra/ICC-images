def scalarProduct2d(arr1, arr2):
    result = 0
    for i in range(len(arr1)):
        for j in range(len(arr1[0])):
            result += arr1[i][j] * arr2[i][j]
    return result

def oneDimensionTo2Dimensions(l, width, height):
    result = []
    listIndex = 0
    for i in range(height):
        line = []
        for j in range(width):
            if listIndex >= len(l):
                for k in range(j, width):
                    line.append((0, 0, 0))
                result.append(line)
                return result
            line.append(l[listIndex])
            listIndex += 1
        result.append(line)
    return result
